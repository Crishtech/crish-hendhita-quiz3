<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
 
 
class DataController extends Controller
	{
		public function index()
		{
			// mengambil data dari table pegawai
			$film = DB::table('film')->get();
	
			// mengirim data pegawai ke view index
			return view('index',['film' => $film]);
	
		}
		
			// method untuk insert data ke table pegawai
		public function store(Request $request)
		{
			// insert data ke table pegawai
			DB::table('film')->insert([
				'pegawai_nama' => $request->nama,
				'pegawai_jabatan' => $request->jabatan,
				'pegawai_umur' => $request->umur,
				'pegawai_alamat' => $request->alamat
			]);
			// alihkan halaman ke halaman pegawai
			return redirect('/index');
		
		}

		// method untuk edit data pegawai
		public function edit($id)
		{
			// mengambil data pegawai berdasarkan id yang dipilih
			$pegawai = DB::table('pegawai')->where('pegawai_id',$id)->get();
			// passing data pegawai yang didapat ke view edit.blade.php
			return view('edit',['pegawai' => $pegawai]);
		
		}
		// update data pegawai
		public function update(Request $request)
		{
			// update data pegawai
			DB::table('pegawai')->where('pegawai_id',$request->id)->update([
				'pegawai_nama' => $request->nama,
				'pegawai_jabatan' => $request->jabatan,
				'pegawai_umur' => $request->umur,
				'pegawai_alamat' => $request->alamat
			]);
			// alihkan halaman ke halaman pegawai
			return redirect('/pegawai');
		}
		// method untuk hapus data pegawai
		public function hapus($id)
		{
			// menghapus data pegawai berdasarkan id yang dipilih
			DB::table('pegawai')->where('pegawai_id',$id)->delete();
				
			// alihkan halaman ke halaman pegawai
			return redirect('/pegawai');
		}
	}

