<html>
<head>
	<title>Data Film</title>
</head>
<body>
	<h3>Data</h3>
 
	<a href="/pegawai/tambah"> + Tambah Pegawai Baru</a>
	
	<br/>
	<br/>
 
	<table border="1">
		<tr>
			<th>Judul</th>
			<th>Deskripsi</th>
			<th>Tahun</th>
			<th>Genre</th>
			<th>Opsi</th>
		</tr>
		@foreach($pegawai as $p)
		<tr>
			<td>{{ $p->judul }}</td>
			<td>{{ $p->deskripsi }}</td>
			<td>{{ $p->tahun }}</td>
			<td>{{ $p->id_genre }}</td>
			<td>
				<a href="/film/edit/{{ $p->id_film }}">Edit</a>
				|
				<a href="/film/hapus/{{ $p->id_film }}">Hapus</a>
			</td>
		</tr>
		@endforeach
	</table>
 
 
</body>
</html>