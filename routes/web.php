<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/register', function () {
    return view('register');
});

Route::get('/welcome', function () {
    return view('welcome');
});
Route::get('/', function () {
    return view('index');
});

//route CRUD
Route::get('/data','DataController@index');
Route::post('/data/tambah','DataController@store');
Route::get('/data/edit/{id}','DataController@edit');
Route::post('/data/update','DataController@update');
Route::get('/data/hapus/{id}','DataController@hapus');